#!/bin/sh

# Exit immediately if a command exits with a non-zero status
set -e

# Test
arch=$(dpkg --print-architecture)
if [ "$arch" = 'amd64' ] && [ $(dpkg -s wine32 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
    echo "Installing wine32 package..."
    dpkg --add-architecture i386 && apt-get update && apt-get -y install wine32
else
    if [ "$arch" = 'arm64' ]; then
        echo "There is no wine32 for arm64. Hyperion can't be used."
        exit 1
    fi
fi

echo "Use hyperion to create PE files"
xvfb-run debian/tests/create-pe-files

echo "Compare real results with expected results"
python3 debian/tests/compare-image.py screenshot-crypted32.png debian/tests/image32.png
python3 debian/tests/compare-image.py screenshot-crypted64.png debian/tests/image64.png
